import os, logging, subprocess
from inotify_simple import INotify, flags

logger = logging.getLogger(__name__)
hdlr = logging.FileHandler('watch.log')
formatter = logging.Formatter('%(asctime)s %(levelname)s %(message)s')
hdlr.setFormatter(formatter)
logger.addHandler(hdlr)
logger.setLevel(logging.DEBUG)

inotify = INotify()
watch_flags = flags.MODIFY
wd = inotify.add_watch('/home/iavian/ws/ws-python/nohup.out', watch_flags)

while True:
    try:
        events = inotify.read()
    except KeyboardInterrupt:
        break
    else:
        logger.debug("Caught %d events", len(events))
        subprocess.run("/usr/bin/curl http://host-check-iavian.appspot.com/notifo?status=wsfailed&sound=tugboat", shell=True)
