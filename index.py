#!/usr/bin/env python3
import sys, signal, socket, logging, pika, threading, json, time, inspect, configargparse, random, string, time

from queue import Queue
from pika import BasicProperties
from configargparse import YAMLConfigFileParser
from yowsup.env import YowsupEnv
from yowsup.layers.axolotl.props import PROP_IDENTITY_AUTOTRUST
from yowsup.stacks import YowStackBuilder
from yowsup.layers import YowLayerEvent, EventCallback
from yowsup.layers.network import YowNetworkLayer
from yowsup.layers.protocol_iq import YowIqProtocolLayer
from yowsup.layers.interface import YowInterfaceLayer, ProtocolEntityCallback
from yowsup.layers.protocol_presence.protocolentities  import SubscribePresenceProtocolEntity
from yowsup.layers.protocol_presence.protocolentities  import UnsubscribePresenceProtocolEntity
from yowsup.layers.protocol_presence.protocolentities  import AvailablePresenceProtocolEntity
from yowsup.layers.protocol_presence.protocolentities  import PresenceProtocolEntity
from yowsup.layers.protocol_profiles.protocolentities  import GetPictureIqProtocolEntity
from yowsup.layers.protocol_contacts.protocolentities.iq_statuses_get import GetStatusesIqProtocolEntity
from yowsup.layers.protocol_contacts.protocolentities.iq_sync_get import GetSyncIqProtocolEntity

# Uncomment to log
logging.basicConfig(level=logging.DEBUG)
logging.getLogger('pika').setLevel(logging.WARNING)
#yowsup-cli registration -r sms --env s40 --mcc 404 --mnc 080 -p 918197729780 --cc 91
#yowsup-cli registration --register 493-301 --env android -p 918197729780 --cc 91

#yowsup-cli registration -r voice --env android --mcc 404 --mnc 45 -p 918197729780 --cc 91
#yowsup-cli registration --register 493-301 --env android -p 918197729780 --cc 91

#yowsup-cli registration -r sms --env android --mcc 404 --mnc 045 -p 918197821293 --cc 91
#yowsup-cli registration --register 493-301 --env android -p 918197729780 --cc 91

class AckLayer(YowInterfaceLayer):
  def receive(self, data):
    try:
      ack_method = getattr(data, "ack")
      if callable(ack_method):
        if len(inspect.getargspec(ack_method).args) == 2:  # 2: self and read
          # send the ack object (True parameter = message was read, not just received)
          logger.info('[Ack] Sending ack - True')
          self.toLower(ack_method(True))
        else:
          logger.info('[Ack] Sending ack')  
          self.toLower(ack_method())
    except AttributeError:
      pass
    self.toUpper(data)

class MyPresenceLayer(YowInterfaceLayer):
  def __init__(self):
    super(MyPresenceLayer, self).__init__()
    self.numberSet = set()
    self.numberSyncSet = set()
    self.connected = False
    threading.Thread(target=self.receive_loop, name="RabbitMQ drain").start()
    threading.Thread(target=self.stats_loop, name="RabbitMQ drain").start()

  def stats_loop(self):
      while True:
          logger.debug('length of queue %d', len(self.numberSet));
          logger.debug(self.numberSet.symmetric_difference(self.numberSyncSet))
          time.sleep(120)

  def receive_loop(self):
      while True:
        if self.connected == False:
          time.sleep(5)
          continue
        qlength = receive_queue.qsize()
        if qlength > 5:
            logger.debug('\033[95mIncoming queue length %d\033[0m', qlength)
        json_obj = receive_queue.get()
        if json_obj.get("action") == "subscribe":
            jsonPayload = json_obj.get("pnumber")
            pnumber = None
            if isinstance(jsonPayload, type([])) == True:
              normalizedjsonPayload = list(map(lambda x : self.normalizeJid(x), jsonPayload))
              duplicates = self.numberSet.intersection(set(normalizedjsonPayload))
              pnumber = set(normalizedjsonPayload) - set(duplicates)
            else:
              normalizedjsonPayload = self.normalizeJid(jsonPayload)
              pnumber = set([normalizedjsonPayload]) - self.numberSet

            if len(pnumber) == 0:
                logger.debug("\033[94mskipping subscribe for number %s\033[0m", pnumber)
                continue
            else:
                logger.debug("\033[93mtrying to sync and subscribe %s\033[0m", pnumber)
                for _pnumber in pnumber:
                  self.numberSet.add(_pnumber)
                self.sendSync(list(pnumber), True)

        elif json_obj.get("action") == "unsubscribe":
            pnumber = self.normalizeJid(json_obj.get("pnumber"))
            isForce = json_obj.get("force", False)
            if isForce == True or pnumber in self.numberSet:
                if pnumber in self.numberSet:
                    self.numberSet.remove(pnumber)
                self.presence_unsubscribe(pnumber)
                logger.debug("trying to unsubscribe %s", pnumber)
            else:
                logger.debug("skipping unsubscribe for number %s", pnumber)
                continue   
        elif json_obj.get("action") == "profilepic":
            pnumber = self.normalizeJid(json_obj.get("pnumber"))
            if pnumber in self.numberSyncSet:
                self.profile_picture(pnumber)
                logger.debug("getting profile picture")
            else:
                logger.debug("skipping profile picture")
        
        if len(self.numberSyncSet) > 1000:
          time.sleep(4)

        time.sleep(3)  
            
  @ProtocolEntityCallback("success")
  def onSuccess(self, successProtocolEntity):
    self.connected = True
    self.toLower(PresenceProtocolEntity(name=config['logfile']))
    self.toLower(AvailablePresenceProtocolEntity())
    
  @ProtocolEntityCallback('presence')
  def onPresenceReceived(self, presence):
    status = False
    lastseen = time.time()
    deny = False

    if presence.getType() is None:
        status = True

    if status == False and (presence.getLast() is None or presence.getLast() is "none" or presence.getLast() is "deny"):
        lastseen = time.time()
        deny = True
    elif status == False:
        lastseen = presence.getLast()

    json_obj = {
        "From": presence.getFrom(),
        "lastseen": int(lastseen),
        "status": status,
        "deny": deny
    }
    send_queue.put(json_obj)
    
  @ProtocolEntityCallback("failure")
  def onFailure(self, entity):
    self.connected = False
    logger.info("Login Failed, reason: %s" % entity.getReason())

  @ProtocolEntityCallback("receipt")
  def onReceipt(self, entity):
    logger.info('[Receipt] Sending receipt')
    self.toLower(entity.ack())

  @ProtocolEntityCallback("event")
  def onEvent(self, layerEvent):
    if layerEvent.getName() == YowNetworkLayer.EVENT_STATE_DISCONNECTED:
        msg = "[Connection] Disconnected reason: %s" % layerEvent.getArg("reason")
        self.connected = False
        logger.warn(msg)

  def presence_subscribe(self, to):  
    presence = SubscribePresenceProtocolEntity(to)
    self.toLower(presence)
    logger.info('\033[95m[Presence] subscribed to %s\033[0m', to)

  def presence_unsubscribe(self, to):
    presence = UnsubscribePresenceProtocolEntity(to)
    self.toLower(presence)
    logger.info('[Presence] unsubscribed to %s', to)
  
  def profile_picture(self, to):
    entity = GetPictureIqProtocolEntity(to, preview=True)
    logger.info('[Profile], getting pic for %s', to)
    self._sendIq(entity, self.onGetContactPictureResult, self.onGetContactPictureError)

  def onGetContactPictureResult(self, result_picture, picture_protocol_entity):
    picData = result_picture.getPictureData().encode('latin-1')
    picFrom = result_picture.getFrom()
    logger.info('[Profile], got pic for %s', result_picture.getFrom())
    pic_queue.put({'pnumber': picFrom, 'payload': picData})

  def onGetContactPictureError(self, iqEntity, picture_protocol_entity):
    logger.warn('[Profile] ERROR getting %s', iqEntity.getFrom(True))
    picFrom = iqEntity.getFrom(True)
    pic_queue.put({'pnumber': picFrom, 'payload': ''})

  def sendSync(self, contacts, delta = False, interactive = True):
    mode = GetSyncIqProtocolEntity.MODE_DELTA if delta else GetSyncIqProtocolEntity.MODE_FULL
    context = GetSyncIqProtocolEntity.CONTEXT_INTERACTIVE if interactive else GetSyncIqProtocolEntity.CONTEXT_REGISTRATION
    #contactEntity = GetSyncIqProtocolEntity(contacts, mode, context)
    contactEntity = GetSyncIqProtocolEntity(contacts)
    self._sendIq(contactEntity, self.onGetSyncResult, self.onGetSyncError)

  def onGetSyncResult(self, resultSyncIqProtocolEntity, originalIqProtocolEntity):
    for k, v in resultSyncIqProtocolEntity.outNumbers.items():
      self.numberSyncSet.add(k)
      self.presence_subscribe(k)

  def onGetSyncError(self, errorSyncIqProtocolEntity, originalIqProtocolEntity):
    logger.error(errorSyncIqProtocolEntity)
    
  def normalizeJid(self, number):
    if '@' in number:
      jid = number
    elif '-' in number:
      jid = "%s@g.us" % number
    else:
      jid = "%s@s.whatsapp.net" % number
    return jid   
  def assertConnected(self):
    if self.connected:
      return True
    else:
      logger.error("Disconnected")
      return False

def do_whatsapp_connection():
    logger.info("Starting whatsapp")
    builder = YowStackBuilder()
    stack = builder\
        .pushDefaultLayers(True)\
        .push(MyPresenceLayer)\
        .push(AckLayer())\
        .build()
    credentials = (config['phone'], config['password'])
    stack.setCredentials(credentials)
    stack.setProp(YowInterfaceLayer.PROP_RECONNECT_ON_STREAM_ERR, False)
    stack.setProp(PROP_IDENTITY_AUTOTRUST, True)
    stack.broadcastEvent(YowLayerEvent(YowNetworkLayer.EVENT_STATE_CONNECT))
    stack.loop()
    
def do_rabbitmq_connection():
    logger.info("Starting MQ")
    def on_open(new_connection):
        new_connection.channel(on_receive_channel_open)
        new_connection.channel(on_send_channel_open)
    def on_send_channel_open(channel):
        def do_send_loop():
            while True:
                json_obj = send_queue.get()
                json_string = json.dumps(json_obj)
                logger.debug("\033[92mSending JSON %s\033[0m", json_string)
                properties = BasicProperties(content_type="application/json",content_encoding="UTF-8")
                channel.basic_publish(exchange="ws_exchange", routing_key="presence", body=json_string, properties=properties)

        threading.Thread(target=do_send_loop, name="RabbitMQ hydrate").start()

        def do_pic_send_loop():
            while True:
                pic_obj = pic_queue.get()
                pnumber = pic_obj["pnumber"]
                payload = pic_obj["payload"]
                logger.debug("Sending pic for %s", pnumber)
                properties = BasicProperties(content_type="application/octet-stream", headers={'pnumber': pnumber})
                channel.basic_publish(exchange="ws_exchange", routing_key="profilepic", body=payload, properties=properties)

        threading.Thread(target=do_pic_send_loop, name="RabbitMQ hydrate").start()

    def on_receive_channel_open(channel):
        def on_queue_declare(result):
            queue_name = result.method.queue
            channel.queue_bind(callback=None, exchange="ws_exchange", queue=queue_name, routing_key=config['routingKey'])
            channel.basic_qos(prefetch_count=1)
            channel.basic_consume(on_incoming, queue=queue_name, no_ack=False)

        def do_receive_bind():
            channel.queue_declare(callback=on_queue_declare, exclusive=False, auto_delete=False, durable=False, queue=config['routingKey'], arguments={'pnumber': config['phone'], 'uptime':config['uptime'], 'host':config['host']})

        def on_incoming(ch, method, properties, body):
            json_obj = json.loads(body.decode('utf8'))
            receive_queue.put(json_obj)
            ch.basic_ack(delivery_tag = method.delivery_tag)

        threading.Thread(target=do_receive_bind, name="RabbitMQ drain").start()

    parameters = pika.URLParameters('amqp://ws:uqs8xmKr2fztVJjN@rmq.whatson-app.net/ws-new?socket_timeout=4000')
    connection = pika.SelectConnection(parameters, on_open)
    try:
        connection.ioloop.start()
    except KeyboardInterrupt:
        connection.close()

if __name__ == "__main__":
    def showConfig(signum, frame):
      logger.debug(config)
    p = configargparse.ArgParser(config_file_parser_class=YAMLConfigFileParser,default_config_files=['config.yaml'])
    p.add('-c', '--config', required=True, is_config_file=True, help='config file path')
    p.add('--phone', required=True, action='append', help='phone')
    p.add('-i', required=True, help='index')
    options = p.parse_args()

    config = {}
    __index  = int(options.i)
    _strPhone = options.phone[__index - 1]
    config['phone'], config['password'], config['routingKey'] = _strPhone.split(',')
    #config['routingKey'] = ''.join(random.choice(string.ascii_uppercase + string.digits) for _ in range(10))
    config['logfile'] = config['routingKey'] + '-' + config['phone'] + '.log'
    config['uptime'] = int(time.time())
    config['host'] = socket.gethostname()

    logger = logging.getLogger(__name__)
    hdlr = logging.FileHandler(config['logfile'])
    formatter = logging.Formatter('%(asctime)s %(levelname)s %(message)s')
    hdlr.setFormatter(formatter)
    logger.addHandler(hdlr)
    logger.setLevel(logging.DEBUG)
    signal.signal(signal.SIGHUP, showConfig)
    logger.debug('Starting with options %s', config)
    receive_queue = Queue()
    send_queue = Queue()
    pic_queue = Queue()

    threading.Thread(target=do_rabbitmq_connection, name="RabbitMQ connection via pika/AQMP").start()
    threading.Thread(target=do_whatsapp_connection, name="WhatsApp connection via yowsup2").start()
