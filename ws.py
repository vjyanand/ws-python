#!/usr/bin/env python3
import sys, os, signal, socket, logging, pika, threading, json, time, inspect, random, string, time

from queue import Queue
from pika import BasicProperties
from yowsup.env import YowsupEnv
from yowsup.layers.axolotl.props import PROP_IDENTITY_AUTOTRUST
from yowsup.stacks import YowStackBuilder
from yowsup.layers import YowLayerEvent, EventCallback
from yowsup.layers.network import YowNetworkLayer
from yowsup.layers.protocol_iq import YowIqProtocolLayer
from yowsup.layers.interface import YowInterfaceLayer, ProtocolEntityCallback
from yowsup.layers.protocol_presence.protocolentities import SubscribePresenceProtocolEntity
from yowsup.layers.protocol_presence.protocolentities import UnsubscribePresenceProtocolEntity
from yowsup.layers.protocol_presence.protocolentities import AvailablePresenceProtocolEntity
from yowsup.layers.protocol_presence.protocolentities import PresenceProtocolEntity
from yowsup.layers.protocol_profiles.protocolentities import GetPictureIqProtocolEntity
from yowsup.layers.protocol_contacts.protocolentities.iq_statuses_get import GetStatusesIqProtocolEntity
from yowsup.layers.protocol_contacts.protocolentities.iq_sync_get import GetSyncIqProtocolEntity

#yowsup-cli registration -r sms --env android --mcc 404 --mnc 080 -p 918667296157 --cc 91
#yowsup-cli registration --register 894-399 --env android -p 918667296157 --cc 91
#{"action":"profilepic", "pnumber":"919986312164"}
#{"action":"subscribe", "pnumber":["917259681100"]}

# Uncomment to log
logging.basicConfig(level=logging.DEBUG)
logging.getLogger('pika').setLevel(logging.WARNING)


class AckLayer(YowInterfaceLayer):

  def receive(self, data):
    try:
      ack_method = getattr(data, "ack")
      if callable(ack_method):
        if len(inspect.getargspec(ack_method).args) == 2:  # 2: self and read
          # send the ack object (True parameter = message was read, not just received)
          logger.info('[Ack] Sending ack - True')
          self.toLower(ack_method(True))
        else:
          logger.info('[Ack] Sending ack')
          self.toLower(ack_method())
    except AttributeError:
      pass
    self.toUpper(data)


class MyPresenceLayer(YowInterfaceLayer):

  def __init__(self):
    super(MyPresenceLayer, self).__init__()
    self.numberSet = set()
    self.numberSyncSet = set()
    self.numberPresenceSet = set()
    self.connected = False
    threading.Thread(target=self.receive_loop, name="RabbitMQ drain").start()
    threading.Thread(target=self.stats_loop, name="RabbitMQ drain").start()

  def stats_loop(self):
    while True:
      logger.info('\033[91mlength of queue %d\033[0m', len(self.numberSet))
      syncDiff = self.numberSet.symmetric_difference(self.numberSyncSet)
      if len(syncDiff) > 0:
        logger.info(syncDiff)
      time.sleep(120)

  def receive_loop(self):
    while True:
      if self.connected == False:
        time.sleep(5)
        continue
      qlength = receive_queue.qsize()
      if qlength > 5:
        logger.info('\033[95mIncoming queue length %d\033[0m', qlength)
      json_obj = receive_queue.get()

      if json_obj.get("action") == "subscribe":
        jsonPayload = json_obj.get("pnumber")
        pnumber = None
        if isinstance(jsonPayload, type([])) == True:
          normalizedjsonPayload = list(map(lambda x: self.normalizeJid(x), jsonPayload))
          duplicates = self.numberSet.intersection(set(normalizedjsonPayload))
          pnumber = set(normalizedjsonPayload) - set(duplicates)
        else:
          normalizedjsonPayload = self.normalizeJid(jsonPayload)
          pnumber = set([normalizedjsonPayload]) - self.numberSet

        if len(pnumber) == 0:
          logger.info("\033[94mskipping subscribe for number %s\033[0m", jsonPayload)
          continue
        else:
          logger.info("\033[93mtrying to sync and subscribe %s\033[0m", pnumber)
          for _pnumber in pnumber:
            self.numberSet.add(_pnumber)
          self.sendSync(list(pnumber), True)

      elif json_obj.get("action") == "presence":
        pnumber = self.normalizeJid(json_obj.get("pnumber"))
        if pnumber in self.numberPresenceSet:
          logger.info("Presence dump %s", json_obj)
          continue
        self.numberPresenceSet.add(pnumber)
        self.presence_subscribe(pnumber)

      elif json_obj.get("action") == "unsubscribe":
        pnumber = self.normalizeJid(json_obj.get("pnumber"))
        isForce = json_obj.get("force", False)
        if isForce == True or pnumber in self.numberSet:
          if pnumber in self.numberSet:
            self.numberSet.remove(pnumber)
            self.presence_unsubscribe(pnumber)
            logger.info("trying to unsubscribe %s", pnumber)
          else:
            logger.info("skipping unsubscribe for number %s", pnumber)

      elif json_obj.get("action") == "profilepic":
        pnumber = self.normalizeJid(json_obj.get("pnumber"))
        if pnumber in self.numberSyncSet:
          self.profile_picture(pnumber)
          logger.info("getting profile picture")
        else:
          logger.info("skipping profile picture")

      if len(self.numberSyncSet) > 1000:
        time.sleep(4)

      if json_obj.get("action") == "presence":
        time.sleep(random.uniform(9.1, 10.9))
      else:
        time.sleep(random.uniform(10.1, 11.0))

  @ProtocolEntityCallback('presence')
  def onPresenceReceived(self, presence):
    status = False
    lastseen = time.time()
    deny = False

    if presence.getType() is None:
      status = True

    if status == False and (presence.getLast() is None or presence.getLast() is "none" or presence.getLast() is "deny"):
      lastseen = time.time()
      deny = True
    elif status == False:
      lastseen = presence.getLast()

    json_obj = {
        "From": presence.getFrom(),
        "lastseen": int(lastseen),
        "status": status,
        "deny": deny
    }
    send_queue.put(json_obj)

  @ProtocolEntityCallback("failure")
  def onFailure(self, entity):
    self.connected = False
    logger.info("Login Failed, reason: %s" % entity.getReason())

  @ProtocolEntityCallback("receipt")
  def onReceipt(self, entity):
    logger.info('[Receipt] Sending receipt')
    self.toLower(entity.ack())

  @ProtocolEntityCallback("success")
  def onSuccess(self, successProtocolEntity):
    logger.info('Connected')
    self.toLower(PresenceProtocolEntity(name='whatsapp'))
    self.toLower(AvailablePresenceProtocolEntity())
    contactEntity = GetSyncIqProtocolEntity([], GetSyncIqProtocolEntity.MODE_FULL, GetSyncIqProtocolEntity.CONTEXT_REGISTRATION)
    time.sleep(2)
    self._sendIq(contactEntity, self.onGetSyncResult, self.onGetSyncError)
    time.sleep(5)
    self.connected = True

  @EventCallback(YowNetworkLayer.EVENT_STATE_DISCONNECTED)
  def onStateDisconnected(self, layerEvent):
    self.connected = False
    logger.error("Disconnected: %s" % layerEvent.getArg("reason"))
    logger.error("System Exit Called")
    os._exit(1)

  def presence_subscribe(self, to):
    presence = SubscribePresenceProtocolEntity(to)
    self.toLower(presence)
    logger.info('\033[95m[Presence] subscribed to %s\033[0m', to)

  def presence_unsubscribe(self, to):
    presence = UnsubscribePresenceProtocolEntity(to)
    self.toLower(presence)
    logger.info('[Presence] unsubscribed to %s', to)

  def profile_picture(self, to):
    entity = GetPictureIqProtocolEntity(to, preview=True)
    logger.info('[Profile], getting pic for %s', to)
    self._sendIq(entity, self.onGetContactPictureResult, self.onGetContactPictureError)

  def onGetContactPictureResult(self, result_picture, picture_protocol_entity):
    picData = result_picture.getPictureData().encode('latin-1')
    picFrom = result_picture.getFrom()
    logger.info('[Profile], got pic for %s', result_picture.getFrom())
    pic_queue.put({'pnumber': picFrom, 'payload': picData})

  def onGetContactPictureError(self, iqEntity, picture_protocol_entity):
    logger.warn('[Profile] ERROR getting %s', iqEntity.getFrom(True))
    picFrom = iqEntity.getFrom(True)
    pic_queue.put({'pnumber': picFrom, 'payload': ''})

  def sendSync(self, contacts, delta=False, interactive=True):
    mode = GetSyncIqProtocolEntity.MODE_DELTA if delta else GetSyncIqProtocolEntity.MODE_FULL
    context = GetSyncIqProtocolEntity.CONTEXT_INTERACTIVE if interactive else GetSyncIqProtocolEntity.CONTEXT_REGISTRATION
    contactEntity = GetSyncIqProtocolEntity(contacts, mode, context)
    self._sendIq(contactEntity, self.onGetSyncResult, self.onGetSyncError)

  def onGetSyncResult(self, resultSyncIqProtocolEntity, originalIqProtocolEntity):
    for k, v in resultSyncIqProtocolEntity.outNumbers.items():
      self.numberSyncSet.add(k)
      json_obj = {'action': 'presence', 'pnumber': k}
      receive_queue.put(json_obj)

  def onGetSyncError(self, errorSyncIqProtocolEntity, originalIqProtocolEntity):
    logger.error(errorSyncIqProtocolEntity)

  def normalizeJid(self, number):
    if '@' in number:
      jid = number
    elif '-' in number:
      jid = "%s@g.us" % number
    else:
      jid = "%s@s.whatsapp.net" % number
    return jid

def do_whatsapp_connection():
  logger.info("Starting whatsapp")
  builder = YowStackBuilder()
  global myPresenceLayer
  myPresenceLayer = MyPresenceLayer()
  stack = builder.pushDefaultLayers(True).push(myPresenceLayer).push(AckLayer()).build()
  credentials = (config['pnumber'], config['password'])
  stack.setCredentials(credentials)
  stack.setProp(PROP_IDENTITY_AUTOTRUST, True)
  stack.broadcastEvent(YowLayerEvent(YowNetworkLayer.EVENT_STATE_CONNECT))
  try:
    stack.loop()
  except KeyboardInterrupt:
    sys.exit(1)


def do_rabbitmq_connection():

  def on_open(new_connection):
    logger.info("[RMQ] Connected")
    new_connection.channel(on_rpc_channel_open)

  def on_send_channel_open(channel):

    def do_send_loop():
      while True:
        json_obj = send_queue.get()
        json_string = json.dumps(json_obj)
        logger.info("\033[92m[RMQ] Sending %s\033[0m", json_string)
        properties = BasicProperties(content_type="application/json", content_encoding="UTF-8")
        channel.basic_publish(exchange="ws_exchange", routing_key="presence", body=json_string, properties=properties)
        config['ptime'] = int(time.time())

    t = threading.Thread(target=do_send_loop, name="RabbitMQ hydrate")
    t.daemon = True
    t.start()

    def do_pic_send_loop():
      while True:
        pic_obj = pic_queue.get()
        pnumber = pic_obj["pnumber"]
        payload = pic_obj["payload"]
        logger.info("[RMQ] Sending pic for %s", pnumber)
        properties = BasicProperties(content_type="application/octet-stream", headers={'pnumber': pnumber})
        channel.basic_publish(exchange="ws_exchange", routing_key="profilepic", body=payload, properties=properties)

    t = threading.Thread(target=do_pic_send_loop, name="RabbitMQ hydrate")
    t.daemon = True
    t.start()

  def on_receive_channel_open(channel):

    def on_queue_declare(result):
      queue_name = result.method.queue
      channel.queue_bind(callback=None, exchange="ws_exchange", queue=queue_name, routing_key=queue_name)
      channel.basic_qos(prefetch_count=1)
      channel.basic_consume(on_incoming, queue=queue_name, no_ack=False)

    def do_receive_bind():
      channel.queue_declare(callback=on_queue_declare, exclusive=False, auto_delete=True, durable=False, queue=config['queue'], arguments={'x-message-ttl': 3600000, 'pnumber': config['pnumber'], 'uptime': config['uptime'], 'host': config['host']})

    def on_incoming(ch, method, properties, body):
      json_obj = json.loads(body.decode('utf8'))
      receive_queue.put(json_obj)
      ch.basic_ack(delivery_tag=method.delivery_tag)

    threading.Thread(target=do_receive_bind, name="RabbitMQ drain").start()

  def on_rpc_channel_open(channel):

    def rpc_callback(ch, method, props, body):
      json_obj = json.loads(body.decode('utf8'))
      logger.debug('Boot broadcast %s', json_obj)
      if json_obj.get('instance') == config['instance']:
        logger.debug('Boot reply %s', json_obj)
        ch.basic_ack(delivery_tag=method.delivery_tag)
        if json_obj.get('action') == 'boot':
          config['pnumber'] = json_obj.get('pnumber')
          config['password'] = json_obj.get('password')
          config['queue'] = json_obj.get('queue')
          if 'pnumber' in config and config['pnumber'] != None:
            connection.channel(on_receive_channel_open)
            connection.channel(on_send_channel_open)
            threading.Thread(target=do_whatsapp_connection, name="WhatsApp connection via yowsup2").start()
          do_ping()
        elif json_obj.get('action') == 'kill':
          pass

    def on_rpc_declare(result):
      queue_name = result.method.queue
      logger.info('[RMQ] Binding RPC queue %s', queue_name)
      config['rpc_channel'] = queue_name
      channel.queue_bind(callback=None, exchange="ws_exchange", queue=queue_name, routing_key=queue_name)
      json_obj = {
        "action": "boot",
        "host": config['host'],
        "instance": config['instance']
      }
      json_string = json.dumps(json_obj)
      channel.basic_qos(prefetch_count=1)
      channel.basic_consume(rpc_callback, queue=queue_name)
      properties = BasicProperties(content_type="application/json", content_encoding="UTF-8", reply_to=queue_name)
      channel.basic_publish(exchange="ws_exchange", routing_key="rpc", body=json_string, properties=properties)
      logger.info("[RMQ] Awaiting message for bootstrap")

    def do_ping():
      json_obj = {}
      if 'pnumber' in config and config['pnumber'] != None:
        json_obj = {
            "action": "ping",
            "host": config['host'],
            "pnumber": config['pnumber'],
            "uptime": config['uptime'],
            "connected": False
        }
        if myPresenceLayer != None:
          json_obj['nlist'] = list(myPresenceLayer.numberSet)
          json_obj['slist'] = list(myPresenceLayer.numberSyncSet)
          json_obj['plist'] = list(myPresenceLayer.numberPresenceSet)
          json_obj['ptime'] = config['ptime']
          json_obj['connected'] = myPresenceLayer.connected
        else:
          logger.debug('myPresenceLayer is None')
        t = threading.Timer(5.0, do_ping)
        t.daemon = True
        t.start()
      else:
        json_obj = {
          "action": "boot",
          "host": config['host'],
          "instance": config['instance']
        }
        time.sleep(6)
      json_string = json.dumps(json_obj)
      if myPresenceLayer != None and len(myPresenceLayer.numberSet) == 0:  
        logger.debug("Config %s", json_string)
      properties = BasicProperties(content_type="application/json", content_encoding="UTF-8", reply_to=config['rpc_channel'])
      channel.basic_publish(exchange="ws_exchange", routing_key="rpc", body=json_string, properties=properties)

    def do_rpc_bind():
      logger.info('[RMQ] RPC queue declare')
      channel.queue_declare(callback=on_rpc_declare, exclusive=True, arguments={'x-message-ttl': 4000})

    do_rpc_bind()

  logger.info("[RMQ] Starting")
  parameters = pika.URLParameters('amqp://ws:uqs8xmKr2fztVJjN@rmq.whatson-app.net/ws?socket_timeout=4000')
  connection = pika.SelectConnection(parameters, on_open)
  try:
    connection.ioloop.start()
  except KeyboardInterrupt:
    connection.close()
    logger.warn('Closing connection')
    sys.exit(1)


if __name__ == "__main__":

  def showConfig(signum, frame):
    logger.info(config)

  config = {}
  config['host'] = socket.gethostname()
  config['instance'] = ''.join(random.choice(string.ascii_uppercase + string.digits) for _ in range(10))
  config['uptime'] = int(time.time())
  config['ptime'] = None
  logger = logging.getLogger(__name__)
  hdlr = logging.FileHandler('ws_' + config['instance'] + '.log')
  formatter = logging.Formatter('%(asctime)s %(levelname)s %(message)s')
  hdlr.setFormatter(formatter)
  logger.addHandler(hdlr)
  logger.setLevel(logging.DEBUG)
  signal.signal(signal.SIGHUP, showConfig)
  logger.info('Starting with options %s', config)
  receive_queue = Queue()
  send_queue = Queue()
  pic_queue = Queue()
  myPresenceLayer = None
  do_rabbitmq_connection()
