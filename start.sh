cd ~iavian/ws
if [ -e /tmp/axotl ]; then
  rm -rf /tmp/axotl
fi
git clone git@gitlab.com:iavian/axotl.git /tmp/axotl
rm -rf ~iavian/.yowsup/*
cp -r /tmp/axotl/* ~iavian/.yowsup/.
if [ -e  /tmp/axotl/ws.py ]; then
  mv /tmp/axotl/ws.py ~iavian/ws/ws.py
fi
nohup python3 ~iavian/ws/ws.py &> ~iavian/ws/nohup.out &